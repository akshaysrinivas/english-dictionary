#import json
""" Actually we can load only methods from a library using from <library> import <method name> """
from json import load
from difflib import get_close_matches

data = load(open("data.json"))

def dictionary(word):

    word = word.lower()
    exist_index = ''

    if word in data:
        return data[word]
    elif len(get_close_matches(word, data.keys())) > 0:
        right_words = get_close_matches(word, data.keys(), n=5)
        user_decision = input(f"Did you mean { right_words[0] } instead? Y - Yes/N - No\n:")
        user_decision = user_decision.lower()
        if user_decision == 'y':
            return data[right_words[0]]
        elif user_decision == 'n':
            return "\nWord does not exist !" 
        else:
            return "Wrong input!"
    else:
        return "Word does not exist !"

in_data = input("\nEnter a word :")
result = dictionary(in_data)
if type(result) == list:
    for i in result:
        print(f"{result.index(i)+1}: { i }")
else:
    print(result)